package com.lastek.libremc;

import com.lastek.libremc.registry.ModItems;
import net.fabricmc.api.ModInitializer;

public class Warps implements ModInitializer {

    public static final String MOD_ID = "warps";

    @Override
    public void onInitialize() {
        ModItems.registerItems();
    }
}
